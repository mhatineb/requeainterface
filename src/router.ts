


import { createRouter, createWebHistory } from 'vue-router';
import Home from '@/views/Home.vue';
import Info from '@/views/Info.vue'
import Connect from '@/views/Connect.vue'
import Reset from '@/views/Reset.vue'
import Test from '@/components/Test.vue'
import TableApn from '@/components/TableApn.vue';

export default createRouter({
    history: createWebHistory(),
    routes:[
        {
            path:'/Home', name: 'Home', component: Home
        },
        {
            path:'/', name: 'Info', component: Info
        },
        {
            path:'/connect', name:'Connect', component: Connect
        },
        {
            path:'/Test', component: Test
        },
        {
            path:'/Reset', name: 'Reset', component: Reset
        },
        {
            path:'/connect/apn', name: 'apm', component: TableApn
        }

    ]
})

